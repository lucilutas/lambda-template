import { getRequestBody } from "../../utils";

export const exampleHandler = (event, context) => {
  console.debug("Event & Context", { event, context });
  context.callbackWaitsForEmptyEventLoop = false;

  const reqBody = getRequestBody(event);
  console.debug(`<< Example >> Request body: ${reqBody}`);
};
