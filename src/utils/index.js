export { get, uniq } from "lodash";

export const getRequestBody = (event) => {
  if (typeof event === "string") {
    return JSON.parse(event.Records[0].body);
  }

  return event;
};
