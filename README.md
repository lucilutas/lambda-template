# AWS Lambda template

---

This template demonstrates how to develop and deploy a functions on AWS Lambda using the traditional Serverless Framework.

## Anatomy of the template

---

```
src/
├─ functions/
│  ├─ example/
|  |  ├─ config.yml
│  │  ├─ index.js
│  ├─ index.js
├─ utils/
│  ├─ index.js
.babelrc
.gitignore
handler.js
package.json
README.md
serverless.yml
webpack.config.js

```

---

- `src/functions/` - directory containing all the functions that the handler will use
- `src/functions/example/config.yml` - config file containing the env vars for the specific lambda function
- `src/utils/` - utility functions directory
- `handler.js` - the main file that will map all the functions
- `serverless.yml` - config file for the lambda functions

## Usage

---

### Deployment

#### Preconditions:

- Use nodejs 12.x or higher
- Install [serverless framework](https://www.serverless.com/framework/docs/providers/aws/guide/installation/)

#### Deployment steps:

- Login to your [AWS Console](https://aws.amazon.com/console/)

- Go to Systems Manager -> Parameter Store in order to add parameters to be used in the functions.

  - naming convention for the parameters: `/<environment>/<PARAMETER_NAME>`
  - eg: `/staging/EXAMPLE_ENV_VAR`

- Create a [new IAM User](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html#id_users_create_console)
  - select _programmatic access_
  - select _attack existing policies directly_ and paste in the policy shown below
  ```JSON
      {
        "Version": "2012-10-17",
        "Statement": [
          {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
              "iam:*",
              "s3:*",
              "apigateway:*",
              "logs:*",
              "lambda:*",
              "cloudformation:*"
            ],
            "Resource": "*"
          },
          {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "ssm:*",
            "Resource": "*"
          }
        ]
      }
  ```
  - save the access keys; we will need them
- Install [AWS SDK](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html)

- Configure the aws-cli (cmd: `aws configure`) to have the access keys from the IAM User created earlier

- `npm install`

- Deployment commands:

  - `npm run deploy:develop` - Deploy on _develop_ environment
  - `npm run deploy:staging` - Deploy on _staging_ environment
  - `npm run deploy:production` - Deploy on _production_ environment
  - Note: These commands will deploy all of the functions. You can deploy a single function by using `--function functionName`
  - (eg: `serverless deploy --stage develop --function functionName`)

After running deploy, you should see output similar to:

```bash
Serverless: Bundling with Webpack...
asset handler.js 3.51 KiB [emitted] (name: handler) 1 related asset
modules by path ./src/ 1.22 KiB
  ./src/functions/index.js 389 bytes [built] [code generated]
  ./src/functions/example/index.js 525 bytes [built] [code generated]
  ./src/utils/index.js 335 bytes [built] [code generated]
./handler.js 289 bytes [built] [code generated]
external "source-map-support/register" 42 bytes [built] [code generated]
webpack compiled successfully in 503 ms
Serverless: Package lock found - Using locked versions
Serverless: Packing external modules: source-map-support@0.5.19
Serverless: Copying existing artifacts...
Serverless: Packaging service...
Serverless: Creating Stack...
Serverless: Checking Stack create progress...
........
Serverless: Stack create finished...
Serverless: Uploading CloudFormation file to S3...
Serverless: Uploading artifacts...
Serverless: Uploading service lambda-template.zip file to S3 (240.49 KB)...
Serverless: Validating template...
Serverless: Updating Stack...
Serverless: Checking Stack update progress...
..............................
Serverless: Stack update finished...
Service Information
service: lambda-template
stage: staging
region: eu-central-1
stack: lambda-template-staging
resources: 11
api keys:
  None
endpoints:
  GET - https://xxxxxxx.execute-api.eu-central-1.amazonaws.com/staging/example
functions:
  example: lambda-template-staging-example
layers:
  None

```

- The lambda functions can be found in the AWS Console, under Lambda

- The logs will be generated automatically and can be viewed in the AWS Console, under Cloud Watch -> Log groups

### How to invoke

#### Direct invocation

- Copy the lambda ARN from the AWS Console -> Lambda

```javascript
import AWS from "aws-sdk";

AWS.config.update({
  accessKeyId: "accessKeyId",
  secretAccessKey: "secretAccessKey",
  httpOptions: { timeout: 360000 },
});

export const invokeLambdaPromise = ({
  functionArn,
  payload,
  invocationType,
}) => {
  const lambda = new AWS.Lambda();
  const lambdaOptions = {
    FunctionName: functionArn,
    InvocationType: invocationType,
    LogType: "Tail",
    Payload: JSON.stringify(payload),
  };
  return new Promise((resolve, reject) => {
    lambda.invoke(lambdaOptions, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
};
```

Then use the `invokeLambdaPromise` function to invoke the lambda from the main API.

The invocation types are 'Event' and 'RequestResponse'.

Eg:

```javascript
await invokeLambdaPromise({
  functionArn:
    "arn:aws:lambda:eu-central-1:xxxxxxx:function:lambda-template-staging-example",
  payload: { a: "1", b: "2" },
  invocationType: "Event",
});
```

#### Invoke using AWS SQS

- Copy the SQS URL from the AWS Console -> SQS

```javascript
import AWS from "aws-sdk";

AWS.config.update({ region: process.env.S3_BUCKET_REGION });

const sqs = new AWS.SQS({ apiVersion: "2012-11-05" });

export const sendPayloadToSQS = ({ qUrl, payload }) =>
  sqs
    .sendMessage({
      QueueUrl: qUrl,
      MessageBody: JSON.stringify(payload),
      DelaySeconds: 0,
    })
    .promise();
```

Then use the `sendPayloadToSQS` function to send a message to the queue from the main API.

Eg:

```javascript
await sendPayloadToSQS({
  qUrl: "https://sqs.eu-central-1.amazonaws.com/xxxxxxx/example-queue",
  payload: { a: "1", b: "2" },
});
```
